
# Activity.txt - A Personal Log In Text

Inspired by [todo.txt](http://todotxt.com), `activity.txt` is a
personal activity log using a text file as its storage medium.  Like
todo.txt, the point of doing this in a text file is the ability to
easily slice and dice it with scripts, edit it with any editor, sync
it everywhere with Dropbox, and so forth.

Note that this README is more of a design document than actual
documentation at present, because there is little to no code here
yet.  Sorry about that.  Look at the list below to see what is
implemented.

- nothing yet :)


## Log format

Lines in the log file that have no leading space are log entries.
Lines that have leading space (2 spaces are recommended) are notes
and/or additional data applying to the log entry immediately above
them.

A log entry looks like this:

    2011/03/15 13:47 (1:00) [+5] myclient Worked on ticket #3456
    
That is: the date and time the entry was logged, an optional number of
hours spent in parentheses, an optional score in square brackets (see
below for a discussion of score), a project identifier (no spaces
allowed), and the rest of the line is the description of the entry.
This format should be easily parseable even without regular
expressions.

Times are in local time and the date/time format is deliberately human
readable instead of something more ISO-like, because it is intended
that you can read or edit the log file by hand if necessary.

### Project identifiers

The project identifier is several things at once.  Primarily, it is a
way to group related activities together for analysis and reporting.
However, the scripts have hooks to treat certain projects specially,
and the intent is that this can be modified or extended on a personal
basis.

As a simple example, one may wish that when you generate a report of
work for a particular client or project, that ticket numbers
referenced in log entries are automatically linked to the project's
bug tracker -- or even that some bit of scripted glue automatically
logs activity to or from the bug tracker so that when you update one
place, the other is updated as well.

Another example that you will (likely) find in this bundle: I use
activity.txt to keep a reading log, so the 'reading' project implies
additional formatting of both the description and the first line of
notes, to allow for extra reportage and linking.

### Score

The idea of scoring activities comes mainly from the
[Printable CEO](http://davidseah.com/2005/09/the-printable-ceo/) by
David Seah.  The general idea is that you come up with a scoring
system ahead of time and then give yourself points whenever you
do something that furthers your goals.  The original Printable CEO
scoring guide is geared toward entrepreneurs (10 points for gaining a
customer, and so forth) but the same principle should work for
whatever your goals are.

Mr. Seah's beautifully designed paper forms, 2011 edition, can be found
[here](http://davidseah.com/node/the-concrete-goals-tracker/), along
with links to some other related forms.

