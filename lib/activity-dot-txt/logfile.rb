
module ActivityDotTxt
  class LogFile
    include Enumerable
    
    def initialize(name="/Users/rob/Dropbox/journal/activity.txt", mode="r")
      @file = File.open(name, mode)
    end

    def each
      while true do
        begin
          line = @file.readline.strip
          while line.empty? or line.start_with?("#")
            line = @file.readline.strip
          end
          while line[-1] == "\\"
            cont = @file.gets
            break if cont.nil?
            line = line[0...-1] + "\n" + cont.strip
          end
          yield LogEntry.new().parse(line)
        rescue EOFError
          return
        end
      end
    end
    
  end
end
