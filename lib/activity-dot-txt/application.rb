#!/usr/bin/env ruby

require 'optparse'
require 'subcommand'

module ActivityDotTxt
  class Application
    include Subcommands
    attr_reader :options, :appname, :plugins, :commands

    # Plugins are simply classes registered here.
    # They will be instantiated and used somehow when the app is started.
    @@plugin_classes = {}
    def self.register_plugin(plugin)
      @@plugin_classes[plugin.name] = plugin
    end
    
    def initialize
      @options = {}
      @appname = File.basename($0)
      @plugins = {}
      @commands = {}
      
      global_options do |opts|
        opts.banner = "Usage: #{appname} [options] [subcommand [options]]"
        opts.description = "Personal activity log."
        opts.separator ""
        opts.separator "Global options:"
        opts.on("-v", "--[no-]verbose", "Verbose output") do |v|
          @options[:verbose] = v
        end
      end
      add_help_option

      @@plugin_classes.each do |name, klass|
        @plugins[name] = klass.new(self)
      end
    end

    def register_command(name, func, &block)
      @commands[name] = func
      command(name, &block)
    end
      
    def run
      cmd = opt_parse()

      if @commands.include?(cmd)
        @commands[cmd].call(self, options, ARGV)
      end
    end
  end
end

