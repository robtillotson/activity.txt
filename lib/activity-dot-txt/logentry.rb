
#
# The basic log entry class.
#

module ActivityDotTxt
  class LogEntry
    attr_accessor :time, :duration, :score, :project, :text

    def initialize
      @time = Time.now
      @duration = nil
      @score = nil
      @project = ""
      @text = ""
    end

    def duration_hms
      hours = @duration/3600.to_i
      mins = (@duration/60 - hours*60).to_i
      secs = (@duration - (mins*60 + hours*3600)).to_i
      return [hours, mins, secs]
    end
    
    def to_s
      ret = @time.strftime("%F %H:%M")
      if not @duration.nil?
        hms = duration_hms
        ret += " (#{hms[0]}:#{hms[1]})"
      end
      ret += " [#{score}]" if not score.nil?
      ret += " " + project
      ret += " " + text.gsub("\n") {"\\\n"}
      ret
    end

    def parse(s)
      pat = /(\d+[-\/]\d+[-\/]\d+)(?:\s+(\d+:\d+))?  (?:\s+\((\d+(?::\d+)?)\))?  (?:\s+\[([-+0-9]+)\])?  \s+(\S+) \s+(.*)$/x

      pat.match(s) do |m|
        d = m[1].split(/[-\/]/).map(&:to_i)
        t = m[2].nil? ? [0,0] : m[2].split(":").map(&:to_i)
        @time = Time.local(*(d+t))
        if not m[3].nil?
          hr = m[3].split(":").map(&:to_i)+[0,0,0]
          @duration = (hr[0]*3600)+(hr[1]*60)+hr[2]
        end
        @score = m[4].to_i if not m[4].nil?
        @project = m[5]
        @text = m[6]
      end
      return self
    end
  end
end
